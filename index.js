console.log("Hello World")


// [SECTION] Arithmetic Operators

let x = 100;
let y = 25;

// Addition
let sum = x + y;
console.log("Result of addition operator: " +sum);


//Substracton

let difference = x-y;
console.log("Result of substraction operator: " + difference);

// Multiplication

let product = x * y;
console.log("Resulf of multiplication operator: " + product);

// Division
let quotient = x / y;
console.log("Result of division operator : " + quotient);

// Modulo
let modulo = x % y;
console.log("Result of modulo operator : "+ modulo);


// [SECTION] Assignment Operator (=)

// basic assignment operator (=)
// the assignment operator assigns the value of the "right hand" operand to a variable.

let assignmentNumber = 8;
console.log("The current value of the assignmentNumber variable: " +assignmentNumber);

// Addition Assignment Operator
assignmentNumber+=2;
console.log("Result of addition assignmentNumber operator: " +assignmentNumber);

// Substraction/Multiplication/Division (-=, *=, /=)
assignmentNumber-=2;
console.log("Result of subtraction assignmentNumber operator: " +assignmentNumber);

assignmentNumber*=2;
console.log("Result of multiplication assignmentNumber operator: " +assignmentNumber);

assignmentNumber/=2;
console.log("Result of division assignmentNumber operator: " +assignmentNumber);

// [Section] PEMDAS
// Multiple Operators and Parenthesis
// When Multiple operators are applied in a single statement it follows PEMDAS.

let mdas= 1+2-3*4/5;
console.log("Results of mdas operation: " +mdas);


// let pemdas = 1+(2-3)*(4/5);

let pemdas = (1+ (2-3))*(4/5);
console.log("Results of pemdas operation: " + pemdas);

// [SECTION] Increment and Decrement
// Operators that add or subtract a values by 1 and reassign the value of the variable where the increment/decrement was applied.

// let b = 0;
let z = 1;
let increment = ++z;
console.log("Result of pre-increment for increment: " +increment);
console.log("Result of Pre-increment for z: " +z);
// console.log("Result of Pre-increment for b: " +b);
// JavaScript read the code from top to bottom and left to right
// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one.
increment = z++;
console.log("Resulf of post-increment: " +increment);
console.log("Result of post-increment for z: " +z);


let decrement = --z;
console.log("Result of pre-decrement: " +decrement);
console.log("Resulf of pre-decrement for z:" +z);

decrement = z--;
console.log("Result of post-decrement: " +decrement);
console.log("Resulf of post-decrement for z:" +z);

// [SECTION] Type Coercion
// is the automatic conversion of values from one data type to another.

let numA = '10';
let numB = 12;

let c = numA + numB;
console.log(c);
console.log(typeof c);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


// The result is a number
// -boolean operator true = 1 false = 0
let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);


// [Section] Comparison operators
// Comparison operators are used to evaluate and compare the left and right operands.
// after evaluation, it returns a boolean value.

let juan = "juan";

// equality operator ()
// Checks wether the operands are equal/have the same content.

console.log(1==1); // true
console.log(1 ==2 ); //false
console.log(1=='1'); //true
console.log (0 == false) //true
console.log ('juan' == 'juan') //true //casesensitive
console.log ('juan' == juan) //True

// Inequality Operator
/*
  -checks whether the operands are not equal/have the different value.
  -attempts to CONVERT AND COMPARE operand of different data type.
  -!=
*/

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(0 != false); //false
console.log('juan' != 'juan'); //false
console.log('juan' != juan); //false


//strict equality operator (===)
// check whether the operands are equal/have the same content.
// Also compares the DATA TYPES of 2 values.

console.log(1===1); //True
console.log(1===2); //False
console.log(1 === '1'); //false
console.log(0 === false); //false
console.log('juan' === 'juan');//true
console.log('juan' === juan);//true


// Strict inequality operator (!==)
// check whether the operands are not equal/does not have the same content.
// Also compares the DATA TYPES of 2 values.

console.log(1!==1); //false
console.log(1!==2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'juan');//false
console.log('juan' !== juan);//false

// [SECTION] Relational Operators
// Some comparison oeprators check whether one values is greater or less than value.

let a = 50;
let b = 65;

// GT or Greater Than operator (>)
let isGreaterThan = a > b;
console.log(isGreaterThan);

// LT or less than Operator (<)
let isLessThan = a<b;
console.log(isLessThan);

//GTE or Greater than or Equal (>=)
let isGTOrEqual = a>=b;
console.log(isGTOrEqual);

//LTE or Less Than or Equal (<=)
let isLTOrEqual = a<=b;
console.log(isLTOrEqual);

let numStr="30";
console.log(a > numStr);

let str="twenty"
console.log(b >=str);//false

// [Section] Logical operators
// Logical operators allow us to be more specific in the logical combination of conditions and evaluations.
// it return boolean.

let isLegalAge = true;
let isRegistered = true;


// Logcal and Operator && - Double Ampersand
// Returns true if all operands are true.

let allRequirementsMet = isLegalAge && isRegistered;

console.log("Result of logical AND operator: " + allRequirementsMet);

// Logical OR Operator (|| - Double)

let someRequirementsMet = isLegalAge || isRegistered
console.log("Result of Logical OR Operator: " + someRequirementsMet)

// Logical Not Operator ! - Exclamation Point ;
// returns the opposite value
let someRequirementsNotMet = !isLegalAge;
console.log ("Results of logical NOT Operator: "+ someRequirementsNotMet)






















//
